/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tbd_i;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Franks
 */
public class Afiliado extends javax.swing.JFrame {
    
    Funciones fn = new Funciones();
    Connection conn = fn.myConnection();
    Object[][] name;
    int transaccion;
    Main_Menu menu;
    
    public Afiliado() throws SQLException {
        initComponents();
        
        int num_cuenta = fn.getNumCuentaAfiliado(conn);
        transaccion = fn.countTransaccion(conn,num_cuenta);
        
        
        lblId.setText(fn.getRegistrosAfiliado(conn, Funciones.current_id, 1));
        lblNombre.setText(fn.getRegistrosAfiliado(conn, Funciones.current_id, 2));
        lblDate.setText(fn.getRegistrosAfiliado(conn, Funciones.current_id, 5));
        
        lblCuenta.setText(fn.getRegistrosCuenta(conn, Funciones.current_id, 1));
        lblAportaciones.setText(fn.getRegistrosCuenta(conn, Funciones.current_id, 3));
        lblActivo1.setText(fn.getRegistrosCuenta(conn, Funciones.current_id, 4));
        lblRetiro.setText(fn.getRegistrosCuenta(conn, Funciones.current_id, 5));      
        lblActivo2.setText(fn.getRegistrosCuenta(conn, Funciones.current_id, 6));
        lblFechaInicio.setText(fn.getRegistrosCuenta(conn, Funciones.current_id, 7));        
                
        lblTipo.setText(fn.getRegistrosPrestamo(conn,Funciones.current_id, 1));
        lblNumero.setText(fn.getRegistrosPrestamo(conn,Funciones.current_id, 2));
        lblSaldo.setText(fn.getRegistrosPrestamo(conn,Funciones.current_id, 3));
        lblMonto.setText(fn.getRegistrosPrestamo(conn,Funciones.current_id, 4));
        lblFecha.setText(fn.getRegistrosPrestamo(conn,Funciones.current_id, 5));

        tblTransaccion = new javax.swing.JTable();
        name = new Object[transaccion][6];
        //tblTransaccion.setModel(new javax.swing.table.DefaultTableModel(name ,new String [] {"Id Transaccion", "Numero Cuenta", "Mes", "Anio","Retiro","Abono"}));
        
        for(int i=0; i <transaccion; i++){
            for(int j=0; j<6; j++){
                try {
                    name[i][j] = fn.getRegistrosTransaccion(conn, 1, j+1);
                } catch (SQLException ex) {
                    Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        tblTransaccion.setModel(new javax.swing.table.DefaultTableModel(name ,new String [] {"Id Transaccion", "Numero Cuenta", "Mes", "Anio","Retiro","Abono"}));
        jScrollPane2.setViewportView(tblTransaccion);

        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jInterface = new javax.swing.JPanel();
        lblId = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btbLogout = new javax.swing.JButton();
        jPrestamo = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        lblTipo = new javax.swing.JLabel();
        lblNumero = new javax.swing.JLabel();
        lblSaldo = new javax.swing.JLabel();
        lblMonto = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        jTransaccion = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblTransaccion = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        jDividendo = new javax.swing.JPanel();
        jCuenta = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        lblCuenta = new javax.swing.JLabel();
        lblAportaciones = new javax.swing.JLabel();
        lblActivo1 = new javax.swing.JLabel();
        lblRetiro = new javax.swing.JLabel();
        lblActivo2 = new javax.swing.JLabel();
        lblFechaInicio = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblId.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        lblId.setText("null");

        lblNombre.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        lblNombre.setText("null");

        lblDate.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        lblDate.setText("null");

        jLabel5.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel5.setText("Id Afiliado: ");

        jLabel6.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel6.setText("Fecha de Inicio:");

        jLabel7.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel7.setText("Nombre Completo:");

        jLabel4.setFont(new java.awt.Font("Consolas", 1, 30)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Bienvenido Afiliado");
        jLabel4.setToolTipText("");

        btbLogout.setText("LogOut");
        btbLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btbLogoutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jInterfaceLayout = new javax.swing.GroupLayout(jInterface);
        jInterface.setLayout(jInterfaceLayout);
        jInterfaceLayout.setHorizontalGroup(
            jInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInterfaceLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jInterfaceLayout.createSequentialGroup()
                        .addGap(278, 278, 278)
                        .addGroup(jInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addGap(131, 131, 131)
                        .addGroup(jInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
                            .addComponent(lblId, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
            .addGroup(jInterfaceLayout.createSequentialGroup()
                .addGap(428, 428, 428)
                .addComponent(btbLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jInterfaceLayout.setVerticalGroup(
            jInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInterfaceLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jLabel4)
                .addGap(83, 83, 83)
                .addGroup(jInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lblId))
                .addGap(54, 54, 54)
                .addGroup(jInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lblNombre))
                .addGap(45, 45, 45)
                .addGroup(jInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(lblDate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addComponent(btbLogout)
                .addGap(76, 76, 76))
        );

        jTabbedPane1.addTab("Interface", jInterface);

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 30)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Prestamos");

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 22)); // NOI18N
        jLabel2.setText("Prestamo Activo:");

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel3.setText("Numero Prestamo");

        jLabel9.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Saldo");

        jLabel10.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel10.setText("Monto");

        jLabel11.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel11.setText("Fecha");

        jLabel12.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel12.setText("Tipo Prestamo");

        lblTipo.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblTipo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTipo.setText("tipo");

        lblNumero.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblNumero.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNumero.setText("numero");

        lblSaldo.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblSaldo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSaldo.setText("saldo");

        lblMonto.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblMonto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMonto.setText("monto");

        lblFecha.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblFecha.setText("fecha");

        javax.swing.GroupLayout jPrestamoLayout = new javax.swing.GroupLayout(jPrestamo);
        jPrestamo.setLayout(jPrestamoLayout);
        jPrestamoLayout.setHorizontalGroup(
            jPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPrestamoLayout.createSequentialGroup()
                .addGroup(jPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPrestamoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPrestamoLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addGroup(jPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblTipo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(45, 45, 45)
                        .addGroup(jPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblNumero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(64, 64, 64)
                        .addGroup(jPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSaldo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE))
                        .addGap(90, 90, 90)
                        .addGroup(jPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPrestamoLayout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPrestamoLayout.createSequentialGroup()
                                .addComponent(lblMonto, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                                .addGap(59, 59, 59)))
                        .addGroup(jPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPrestamoLayout.setVerticalGroup(
            jPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPrestamoLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel1)
                .addGap(49, 49, 49)
                .addGroup(jPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(jPrestamoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTipo)
                    .addComponent(lblNumero)
                    .addComponent(lblSaldo)
                    .addComponent(lblMonto)
                    .addComponent(lblFecha))
                .addContainerGap(284, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Prestamo", jPrestamo);

        tblTransaccion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblTransaccion);

        jLabel13.setFont(new java.awt.Font("Consolas", 1, 30)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Transaccion");

        javax.swing.GroupLayout jTransaccionLayout = new javax.swing.GroupLayout(jTransaccion);
        jTransaccion.setLayout(jTransaccionLayout);
        jTransaccionLayout.setHorizontalGroup(
            jTransaccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jTransaccionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jTransaccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1034, Short.MAX_VALUE))
                .addContainerGap())
        );
        jTransaccionLayout.setVerticalGroup(
            jTransaccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jTransaccionLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Transaccion", jTransaccion);

        javax.swing.GroupLayout jDividendoLayout = new javax.swing.GroupLayout(jDividendo);
        jDividendo.setLayout(jDividendoLayout);
        jDividendoLayout.setHorizontalGroup(
            jDividendoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1046, Short.MAX_VALUE)
        );
        jDividendoLayout.setVerticalGroup(
            jDividendoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 475, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Dividendo", jDividendo);

        jLabel8.setFont(new java.awt.Font("Consolas", 1, 30)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Estado de Cuenta Afiliado");

        jLabel14.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel14.setText("Cuenta : ");

        jLabel15.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel15.setText("Numero Cuenta");

        jLabel17.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel17.setText("Aportaciones");

        jLabel18.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel18.setText("Activo");

        jLabel19.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel19.setText("Activo");

        jLabel20.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel20.setText("Retiro");

        jLabel21.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel21.setText("Fecha Inicio");

        lblCuenta.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblCuenta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCuenta.setText("jLabel16");

        lblAportaciones.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblAportaciones.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAportaciones.setText("jLabel22");

        lblActivo1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblActivo1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblActivo1.setText("jLabel23");

        lblRetiro.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblRetiro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblRetiro.setText("jLabel24");

        lblActivo2.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblActivo2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblActivo2.setText("jLabel25");

        lblFechaInicio.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblFechaInicio.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblFechaInicio.setText("jLabel26");

        javax.swing.GroupLayout jCuentaLayout = new javax.swing.GroupLayout(jCuenta);
        jCuenta.setLayout(jCuentaLayout);
        jCuentaLayout.setHorizontalGroup(
            jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jCuentaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jCuentaLayout.createSequentialGroup()
                        .addGroup(jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jCuentaLayout.createSequentialGroup()
                                .addGap(151, 151, 151)
                                .addGroup(jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblCuenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(51, 51, 51)
                        .addGroup(jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jCuentaLayout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addGap(53, 53, 53)
                                .addGroup(jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblActivo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(lblAportaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(60, 60, 60)
                        .addGroup(jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addComponent(lblRetiro))
                        .addGap(44, 44, 44)
                        .addGroup(jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblActivo2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(51, 51, 51)
                        .addGroup(jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21)
                            .addComponent(lblFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 62, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jCuentaLayout.setVerticalGroup(
            jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jCuentaLayout.createSequentialGroup()
                .addGap(65, 65, 65)
                .addComponent(jLabel8)
                .addGap(49, 49, 49)
                .addGroup(jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18)
                    .addComponent(jLabel19)
                    .addComponent(jLabel20)
                    .addComponent(jLabel21))
                .addGap(34, 34, 34)
                .addGroup(jCuentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(lblCuenta)
                    .addComponent(lblAportaciones)
                    .addComponent(lblActivo1)
                    .addComponent(lblRetiro)
                    .addComponent(lblActivo2)
                    .addComponent(lblFechaInicio))
                .addContainerGap(247, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cuenta", jCuenta);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btbLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btbLogoutActionPerformed
        Funciones.Current_User = null;
        Funciones.current_id = -1;
        menu = new Main_Menu();
        Funciones.centreWindow(menu);
        menu.setVisible(true);                    
        this.dispose();
    }//GEN-LAST:event_btbLogoutActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btbLogout;
    private javax.swing.JPanel jCuenta;
    private javax.swing.JPanel jDividendo;
    private javax.swing.JPanel jInterface;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPrestamo;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel jTransaccion;
    private javax.swing.JLabel lblActivo1;
    private javax.swing.JLabel lblActivo2;
    private javax.swing.JLabel lblAportaciones;
    private javax.swing.JLabel lblCuenta;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblFechaInicio;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblMonto;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblNumero;
    private javax.swing.JLabel lblRetiro;
    private javax.swing.JLabel lblSaldo;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JTable tblTransaccion;
    // End of variables declaration//GEN-END:variables
}
