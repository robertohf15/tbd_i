/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tbd_i;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.*;

public class Funciones {
    
    
    static String Current_User = null;     
    static int current_id = -1;
    
    static int current_Id_Afiliado = 0;
    static int current_Num_Cuenta= 0;
    
    public static void centreWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }
    
    public Connection myConnection(){
        //String jdbcClassName="com.ibm.db2.jcc.DB2Driver";
        String url="jdbc:db2://localhost:50000/TDBD_I";
        String user="FRANKS";
        String password="holahola";
 
        Connection connection = null;
        try {

            //Class.forName(jdbcClassName);

            connection = DriverManager.getConnection(url, user, password);
 
        } catch (SQLException e) {
        }finally{
            if(connection!=null){
                System.out.println("Connected successfully.");
                return connection;
                
            }else
                return null;
        
        }
    }
    
    public int getIdAfiliado(Connection conn, String username) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        int codigo = -1;
        
        cstmt = conn.prepareStatement("SELECT ID_AFILIADO FROM AFILIADO WHERE TIPO_USUARIO = ?");
        cstmt.setString(1, username);
        rst = cstmt.executeQuery();
        if(rst.next()){
            codigo = rst.getInt("ID_AFILIADO");
        }
        
        return codigo;
    }
    
    public String getTelefonoAfiliado(Connection conn, int id) throws SQLException{
        
        String telefono = "";
        
        PreparedStatement cstmt;
        ResultSet rst;
        
        cstmt = conn.prepareStatement("SELECT TELEFONO FROM TELEFONOS WHERE ID_AFILIADO = ?");
        cstmt.setInt(1, id);
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            telefono = rst.getString("Telefono");
        }
            
        return telefono;
    }
    
    public String getRegistroAfiliado(Connection conn, int id, int opcion) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        
        String registro = null;
        
        cstmt = conn.prepareStatement("CALL SP_AFILIADO_READ(?)");
        cstmt.setInt(1, id);
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            switch(opcion){
                case 1:
                  registro = Integer.toString(rst.getInt("ID_AFILIADO"));
                break;
                
                case 2:
                  registro = (rst.getString("PRIMER_NOMBRE")+" "+rst.getString("PRIMER_APELLIDO"));
                break;
                
                case 3:
                  registro = rst.getString("FECHA_INICIO");
                break;
                
                case 4:
                  registro = getTelefonoAfiliado(conn,id);
                break;
            }
        }
        return registro;
    }
    
    public String getRegistrosAfiliado(Connection conn, int id, int opcion) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        
        String registro = null;
        
        cstmt = conn.prepareStatement("CALL SP_AFILIADO_READ(?)");
        cstmt.setInt(1, id);
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            switch(opcion){
                case 1:
                  registro = Integer.toString(rst.getInt("ID_AFILIADO"));
                break;
                
                case 2:
                  registro = (rst.getString("PRIMER_NOMBRE")+" "+rst.getString("SEGUNDO_NOMBRE")+" "+rst.getString("PRIMER_APELLIDO")+" "+rst.getString("SEGUNDO_APELLIDO"));
                break;
                
                case 3:
                  registro = rst.getString("TIPO_USUARIO");
                break;
                
                case 4:
                  registro = rst.getString("CLAVE");
                break;
                
                case 5:
                  registro = rst.getString("FECHA_INICIO");
                break;
                
            }
        }
        return registro;
    }
    
    public int getMaxIdAfiliado(Connection conn) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        int count = -1;
        
        cstmt = conn.prepareStatement("SELECT MAX(ID_AFILIADO) FROM AFILIADO");
            
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            count = rst.getInt(1);
        }
        return current_Id_Afiliado = count + 1;
    }
    
    public int getMaxNumCuenta(Connection conn) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        int count = -1;
        
        cstmt = conn.prepareStatement("SELECT MAX(NUMERO_CUENTA) FROM CUENTA");
            
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            count = rst.getInt(1);
        }
        return current_Num_Cuenta = count + 1;
    }
    
    public void setRegistroAfiliado(Connection conn, String p_n, String s_n, String p_a, String s_a, String user, String pass,String date1,String calle1, String colonia1, String ciudad1, String num_casa1, String departamento1) throws SQLException{
        
        ResultSet rst;
        PreparedStatement cstmt;
        cstmt = conn.prepareStatement("CALL SP_AFILIADO_CREATE(?,?,?,?,?,?,?,?,?,?,?,?,?)");
        cstmt.setInt(1, current_Id_Afiliado);
        cstmt.setString(2,p_n);
        cstmt.setString(3,s_n);
        cstmt.setString(4,p_a);
        cstmt.setString(5,s_a);
        cstmt.setString(6,user);
        cstmt.setString(7,pass);
        cstmt.setString(8,date1);
        cstmt.setString(9,calle1);
        cstmt.setString(10,colonia1);
        cstmt.setString(11,ciudad1);
        cstmt.setString(12,num_casa1);
        cstmt.setString(13,departamento1);
        cstmt.executeUpdate();
        cstmt.close();
    }
    
    public void setTelefonoAfiliado(Connection conn, String telefono1) throws SQLException{
        
        PreparedStatement cstmt;
        cstmt = conn.prepareCall("CALL SP_TELEFONO_CREATE(?,?)");
        cstmt.setInt(1, current_Id_Afiliado);
        cstmt.setString(2, telefono1);
        cstmt.executeUpdate();
        //cstmt = conn.prepareStatement("CALL SP_TELEFONO_CREATE(?,?)");
        //cstmt.setInt(1,current_ID_afiliado);
        //cstmt.setString(2,telefono);
        //cstmt.executeQuery(); 
        cstmt.close();
        
    }
    
    public void setEmailAfiliado(Connection conn, String email_1) throws SQLException{
        
        PreparedStatement cstmt;
        
        cstmt = conn.prepareStatement("CALL SP_CORREO_ELECTRONICOS_CREATE(?,?)");
        cstmt.setInt(1,current_Id_Afiliado);
        cstmt.setString(2,email_1);
        cstmt.executeUpdate(); 
        cstmt.close();
        
    }
    
    public void setCuentaAfiliado(Connection conn) throws SQLException{
        
        PreparedStatement cstmt;
        
        cstmt = conn.prepareStatement("CALL SP_CUENTA_CREATE(?,?,?,?,?,?)");
        cstmt.setInt(1,current_Num_Cuenta);
        cstmt.setInt(2,current_Id_Afiliado);
        cstmt.setInt(3,200);
        cstmt.setString(4,"Activo");
        cstmt.setInt(5,200);
        cstmt.setString(6,"Activo");     
        cstmt.executeUpdate();
        cstmt.close();
        
    }
    
    public int getNumCuentaAfiliado(Connection conn) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        int Numero_cuenta = -1;
        
        cstmt = conn.prepareStatement("SELECT NUMERO_CUENTA FROM CUENTA WHERE ID_AFILIADO = ?");
        cstmt.setInt(1, current_id);
        rst = cstmt.executeQuery();
        if(rst.next()){
            Numero_cuenta = rst.getInt("NUMERO_CUENTA");
        }
        
        return Numero_cuenta;
    }
    
    public String getRegistrosCuenta(Connection conn, int num_cuenta, int opcion) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        
        String registro = "";
        
        cstmt = conn.prepareStatement("CALL SP_CUENTA_READ(?)");
        cstmt.setInt(1, num_cuenta);
        rst = cstmt.executeQuery();
        if(rst.next()){
            switch(opcion){
                case 1:
                  registro = Integer.toString(rst.getInt("NUMERO_CUENTA"));
                break;

                case 2:
                  registro = Integer.toString(rst.getInt("ID_AFILIADO"));
                break;

                case 3:
                  registro = Integer.toString(rst.getInt("SALDO_APORTACIONES"));
                break;

                case 4:
                  registro = rst.getString("T_APORTACIONES");
                break;

                case 5:
                  registro = Integer.toString(rst.getInt("SALDO_RETIRABLE"));
                break;

                case 6:
                  registro = rst.getString("T_RETIRABLE");
                break;

                case 7:
                  registro = rst.getString("FECHA_INICIO");
                break;

                case 8:
                  registro = rst.getString("ANITUGUEDAD");
                break;

            }
        }
        return registro;
    }
     
    public String getRegistrosPrestamo(Connection conn, int num_cuenta, int opcion) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        
        String registro = "";
        
        cstmt = conn.prepareStatement("CALL SP_PRESTAMO_READ(?)");
        cstmt.setInt(1, num_cuenta);
        rst = cstmt.executeQuery();
        if(rst.next()){
            switch(opcion){
                case 1:
                  registro = Integer.toString(rst.getInt("NUMERO_PRESTAMO"));
                break;

                case 2:
                  registro = rst.getString("TIPO_PRESTAMO");
                break;

                case 3:
                  registro = Integer.toString(rst.getInt("SALDO"));
                break;

                case 4:
                  registro = Integer.toString(rst.getInt("MONTO"));
                break;

                case 5:
                  registro = rst.getString("FECHA");
                break;

            }
        }
        return registro;
    }
    
    public String getRegistrosTransaccion(Connection conn, int num_cuenta, int opcion) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        
        String registro = "";
        
        cstmt = conn.prepareStatement("CALL SP_TRANSACCION_READ(?)");
        cstmt.setInt(1,num_cuenta);
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            switch(opcion){
                case 1:
                  registro = Integer.toString(rst.getInt("ID_TRANSACCION"));
                break;

                case 2:
                  registro = Integer.toString(rst.getInt("NUMERO_CUENTA"));
                break;

                case 3:
                  registro = rst.getString("MES");
                break;

                case 4:
                  registro = rst.getString("ANIO");
                break;

                case 5:
                  registro = Integer.toString(rst.getInt("MONTO_RETIRAR"));
                break;
                
                case 6:
                  registro = Integer.toString(rst.getInt("MONTO_DEPOSITAR"));
                break;

            }
        }
        return registro;
    }
    
    public int countTransaccion(Connection conn, int num_cuenta) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        int count = -1;
        
            cstmt = conn.prepareStatement("SELECT COUNT(*) FROM TRANSACCION WHERE NUMERO_CUENTA = ?");
            cstmt.setInt(1, num_cuenta);
            
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            count = rst.getInt(1);
        }
        return count;
    }
    
    public int countTransacciones(Connection conn) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        int count = -1;
        
        cstmt = conn.prepareStatement("SELECT COUNT(*) FROM TRANSACCION");
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            count = rst.getInt(1);
        }
        return count;
    }

    public int countCuentas(Connection conn) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        int count = -1;
        
        cstmt = conn.prepareStatement("SELECT COUNT(*) FROM CUENTA");
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            count = rst.getInt(1);
        }
        return count;
    }

    public int countPrestamos(Connection conn) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        int count = -1;
        
        cstmt = conn.prepareStatement("SELECT COUNT(*) FROM PRESTAMO");
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            count = rst.getInt(1);
        }
        return count;
    }
    
    public int countAfiliados(Connection conn) throws SQLException{
        
        PreparedStatement cstmt;
        ResultSet rst;
        int count = -1;
        
        cstmt = conn.prepareStatement("SELECT COUNT(*) FROM AFILIADO");
        rst = cstmt.executeQuery();
        
        if(rst.next()){
            count = rst.getInt(1);
        }
        return count;
    }
}
