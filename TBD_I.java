package tbd_i;
 
import java.sql.Connection;
import java.sql.SQLException;

public class TBD_I {
    
    public static void main(String[] args) throws SQLException {
        
        Funciones fn = new Funciones();
        Connection conn = fn.myConnection();
        
        System.out.print(fn.getMaxIdAfiliado(conn));
        System.out.print(fn.getMaxNumCuenta(conn));
        
        Main_Menu main_menu = new Main_Menu();
        Funciones.centreWindow(main_menu);
        main_menu.setVisible(true);
        
    }
    
}
